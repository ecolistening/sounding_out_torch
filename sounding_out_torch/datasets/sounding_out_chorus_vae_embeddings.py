import attr
import einops
import torch
import pandas as pd
from torch import Tensor
from torch.distributions.normal import Normal
from pathlib import Path
from pandas.api.types import is_categorical_dtype, is_object_dtype
from conduit.data.datasets.base import CdtDataset
from typing import Union, Optional, List

class SoundingOutChorusVAEEmbeddings(CdtDataset):
    _FEATURES_FILENAME = "features.parquet"
    _METADATA_FILENAME = "metadata.parquet"

    def __init__(
        self,
        root: str,
        latent_dim: int = 128,
        num_samples_per_sequence: int = 1,
    ) -> None:
        self.latent_dim = int(latent_dim)
        self.num_samples_per_sequence = int(num_samples_per_sequence)
        self.base_dir = Path(root).expanduser()

        features = pd.read_parquet(self.base_dir / self._FEATURES_FILENAME).iloc[:, 1:]
        metadata = pd.read_parquet(self.base_dir / self._METADATA_FILENAME)

        self.features, self.metadata = features.align(metadata, join='inner', axis=0)
        self.features = self.features.reset_index(drop=True)
        self.metadata = self.metadata.reset_index(drop=True)

        x = einops.rearrange(torch.tensor(self.features.to_numpy()), 'n (seq ld) -> n seq ld', ld=self.latent_dim * 2)
        super().__init__(x=x, y=None, s=torch.tensor(self.metadata.index))

    def _sample_x(self, idx, **kwargs) -> Tensor:
        q_z = self.x[idx]
        mu_x, log_sigma_x = q_z.chunk(2, dim=-1)
        mu_x = einops.repeat(mu_x, 'seq ld -> n seq ld', n=self.num_samples_per_sequence, ld=self.latent_dim)
        sigma_x = einops.repeat((0.5 * log_sigma_x).exp(), 'seq ld -> n seq ld', n=self.num_samples_per_sequence, ld=self.latent_dim)
        return Normal(mu_x, sigma_x).rsample()

    def _sample_y(self, idx, **kwargs) -> Tensor:
        return einops.repeat(self.x[idx], 'seq ld -> n seq ld', n=self.num_samples_per_sequence)

if __name__ == "__main__":
    dataset = SoundingOutChorusVAEEmbeddings(
        root=Path().home() / 'data' / 'sounding_out_chorus_vae_embeddings',
        latent_dim=128,
        num_samples_per_sequence=32,
    )
    print(dataset[0][0].shape)
