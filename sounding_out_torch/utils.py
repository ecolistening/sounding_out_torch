def try_or(fn, default=None):
    try:
        return fn()
    except:
        return default
