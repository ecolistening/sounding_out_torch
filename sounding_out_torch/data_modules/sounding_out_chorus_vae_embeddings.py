import attr
from dataclasses import dataclass
from conduit.data import TernarySample, TrainValTestSplit
from conduit.data.datasets.utils import CdtDataLoader
from conduit.data.datamodules.base import CdtDataModule
from pathlib import Path
from torch.utils.data import Sampler
from typing import (
    Any,
    List,
    Optional,
    Sequence,
)

from sounding_out_torch.datasets.sounding_out_chorus_vae_embeddings import SoundingOutChorusVAEEmbeddings

@attr.define(kw_only=True)
class SoundingOutChorusVAEEmbeddingsDataModule(CdtDataModule):
    root: str = attr.field(kw_only=False)
    latent_dim: float = 128
    num_samples_per_sequence: int = 16

    @staticmethod
    def _batch_converter(batch: TernarySample) -> TernarySample:
        return TernarySample(x=batch.x, y=batch.y, s=batch.s)

    def make_dataloader(
        self,
        ds: SoundingOutChorusVAEEmbeddings,
        *,
        batch_size: int,
        shuffle: bool = False,
        drop_last: bool = False,
        batch_sampler: Optional[Sampler[Sequence[int]]] = None,
    ) -> CdtDataLoader[TernarySample]:
        return CdtDataLoader(
            ds,
            batch_size=batch_size if batch_sampler is None else 1,
            shuffle=shuffle,
            num_workers=self.num_workers,
            pin_memory=self.pin_memory,
            drop_last=drop_last,
            persistent_workers=self.persist_workers,
            batch_sampler=batch_sampler,
            converter=self._batch_converter,
        )

    def predict_dataloader(self):
        eval_train_dataloader = self.make_dataloader(batch_size=self.eval_batch_size, ds=self.train_data)
        return [
            eval_train_dataloader,
            self.val_dataloader(),
            self.test_dataloader(),
        ]

    def _get_splits(self) -> TrainValTestSplit:
        data = SoundingOutChorusVAEEmbeddings(
            root=self.root,
            latent_dim=self.latent_dim,
            num_samples_per_sequence=self.num_samples_per_sequence,
        )
        val, test, train = data.random_split(props=(self.val_prop, self.test_prop), seed=self.seed)
        return TrainValTestSplit(train=train, val=val, test=test)

if __name__ == "__main__":
    dm = SoundingOutChorusVAEEmbeddingsDataModule(
        root=Path().home() / 'data' / 'sounding_out_chorus_vae_embeddings',
        latent_dim=128,
        num_samples_per_sequence=16,
        training_mode="step",
        train_batch_size=64,
        eval_batch_size=64,
        val_prop=0.2,
        test_prop=0.2,
        num_workers=48,
        seed=42,
        persist_workers=False,
        stratified_sampling=False,
        instance_weighting=False,
    )
    dm.setup()
    import code; code.interact(local=locals())
